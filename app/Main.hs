{-# LANGUAGE TypeOperators #-}

module Main where

import RIO
import Magicbane

type MyAppContext = (ModLogger, ModMetrics)
type MyApp = RIO MyAppContext

type HelloRoute = "hello" :> QueryParam "to" Text :> Get '[PlainText] Text

type ExampleAPI = HelloRoute

exampleAPI = Proxy :: Proxy ExampleAPI

hello :: Maybe Text -> MyApp Text
hello x = timed "hello" $ do
  let x' = fromMaybe "anonymous" x
  logInfo $ "Saying hello to " <> display x'
  return $ "Hello " <> x' <> "!"

main = do
  (_, modLogg) <- newLogger (LogStderr defaultBufSize) simpleFormatter
  metrStore <- serverMetricStore <$> forkMetricsServer "0.0.0.0" 8800
  modMetr <- newMetricsWith metrStore
  let ctx = (modLogg, modMetr)
  defWaiMain $ magicbaneApp exampleAPI EmptyContext ctx hello
