# rio-etc-scotty-template

A comprehensive artifact server example utilizing the
[RIO](https://hackage.haskell.org/package/rio) prelude, the
[etc](https://hackage.haskell.org/package/etc) configuration manager and the
[scotty](https://hackage.haskell.org/package/scotty) REST server library. This
builds in gitlab's ci to produce an output binary.

## Building

Build with

    stack build

## Running

Make an uploads directory in advance

    mkdir uploads

Then run with

    stack exec -- app-exe run

You should be able to then curl files to the server that will appear in the uploads directory.

    curl -X POST --form file=@stack.yaml http://localhost:3000/upload

Remember the '@' before the filename is important.

You can then download the files from the browser by navigating to

    http://localhost:3000/get/stack.yaml

It also has a basic search feature

    http://localhost:3000/search/ack
