---
title: Getting Started
---

You can run magicbane-template with

```bash
app-exe
```

You can then visit your browser at http://localhost:3000/hello/foo
