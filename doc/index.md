---
title: Home
---

# magicbane-template - A simple REST server using magicbane

This is the soft documentation site for the magicbane-template.

This serves as a place to provide detailed descriptions about how to
use your application or library.

![build](https://gitlab.com/zenhaskell/magicbane-template/badges/master/build.svg)
